package lab4;

public class Ulamek {
	private int licznik, mianownik;
	public Ulamek kopia;
	public Ulamek getKopia() {
		return kopia;
	}
	public void setKopia(Ulamek kopia) {
		this.kopia = kopia;
	}
		// gettery i settery
		public int getLicznik() {return licznik;}
		public void setLicznik(int licznik) {this.licznik = licznik;}
		public int getMianownik() {return mianownik;}
		public void setMianownik(int mianownik) {this.mianownik = mianownik;}
	// ----------------

	Ulamek(int l, int m) {
		licznik = l;
		mianownik = m;
	}
	Ulamek() {
		setLicznik(0);
		setMianownik(0);
	}
	static Ulamek razy(Ulamek u, Ulamek v)
	{
		Ulamek nowy = new Ulamek(0,0);
		nowy.licznik = (u.getLicznik()) * (v.getLicznik());
		nowy.mianownik = u.mianownik * v.mianownik;

		return nowy;
	}
	// mnozPrzez ulamek
	void mnozPrzez(Ulamek v) {
		
		kopia = this;
		
		licznik = licznik * v.getLicznik();
		mianownik = mianownik * v.getMianownik();
	}
	// mnozPrzez liczbe
	void mnozPrzez(int i) {
		
		kopia = new Ulamek();
		kopia.setLicznik(this.getLicznik());
		kopia.setMianownik(this.getMianownik());
		licznik = licznik * i;
	}

	public String toString() {
		return (licznik + "/" + mianownik);
	}


	/*
	public static void main(String[] args) {
		Ulamek u = new Ulamek(1,2);
		Ulamek v = new Ulamek(2,3);
		Ulamek w = Ulamek.razy(u,v);

		u.mnozPrzez(v);
		u.mnozPrzez(5);

		System.out.println(u);

		System.out.println(w);
	}*/
}
