package lab4;


public class LiczbaU {

	private int calosci;
	private Ulamek czescU;


	LiczbaU(int cal, Ulamek czUlamk) {
		calosci = cal;
		czescU = czUlamk;
	}
	// gettery i settery
	public int getCalosci() {return calosci;}
	public void setCalosci(int calosci) {this.calosci = calosci;}
	public Ulamek getCzescU() {	return czescU;}
	public void setCzescU(Ulamek czescU) {this.czescU = czescU;}
	//----------------- 


	void ulamek_wlasciwy() {
		calosci += czescU.getLicznik() / czescU.getMianownik();
		czescU.setLicznik(czescU.getLicznik() % czescU.getMianownik());
	}

	void ulamek_niewlasciwy() {
		if (this.calosci != 0) {
			czescU.setLicznik(czescU.getMianownik() * calosci + czescU.getLicznik());
			calosci = 0;
		}
	}

	LiczbaU mnozPrzez(LiczbaU l) {
		LiczbaU nowy = new LiczbaU(0, czescU);

		this.ulamek_niewlasciwy();
		l.ulamek_niewlasciwy();
		nowy.czescU.setLicznik(l.czescU.getLicznik() * czescU.getLicznik());
		nowy.czescU.setMianownik(l.czescU.getMianownik() * czescU.getMianownik());
		// wykonaj normalizacje ulamka
		l.ulamek_wlasciwy();
		nowy.ulamek_wlasciwy();

		return nowy;
	}

	LiczbaU mnozPrzez(int i) {
		LiczbaU nowy = new LiczbaU(0, czescU);
		nowy.czescU.setLicznik(nowy.czescU.getLicznik() * i);
		nowy.ulamek_wlasciwy();

		return nowy;
	}

	LiczbaU mnozPrzez(Ulamek u) {
		LiczbaU nowy = new LiczbaU(0, czescU);
		this.ulamek_niewlasciwy();

		nowy.czescU.setLicznik(nowy.czescU.getLicznik() * u.getLicznik());
		nowy.czescU.setMianownik(nowy.czescU.getMianownik() * u.getMianownik());

		nowy.ulamek_wlasciwy();

		return nowy;

	}

	public String toString() {
		return ("" + calosci + " + " + czescU.getLicznik() + "/" + czescU.getMianownik());
	}

	public static void main(String[] args) {
		UlamekZP pierwszy = new UlamekZP(1,2);
		System.out.print("ułamek pierwszy PRZED pomnożeniem: ");
		System.out.println(pierwszy);
		
		pierwszy.mnozPrzez(5);
		
		System.out.print("ułamek pierwszy PO pomnożeniu: ");
		System.out.println(pierwszy);
		
		pierwszy.cofnij();
		
		System.out.print("ułamek pierwszy po COFNIĘCIU: ");
		System.out.println(pierwszy);
		
		pierwszy.cofnij();
		System.out.println();
		
		UlamekZP drugi = new UlamekZP(3,4);
		System.out.print("ułamek drugi PRZED pomnożeniem: ");
		System.out.println(drugi);
		drugi.mnozPrzez(4);
		System.out.print("ułamek drugi PO pomnożeniu: ");
		System.out.println(drugi);
		drugi.cofnij();
		System.out.print("ułamek drugi PO COFNIĘCIU: ");
		System.out.println(drugi);
		
		drugi.cofnij();
	}
}
